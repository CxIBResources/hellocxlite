- Update repository image references to target latest/test URLs for CxLite and CxEngine.

    Sample Value(s):
        name: registry1.dso.mil/ironbank/checkmarx/cxlite/cxlite:1.0
        name: registry.gitlab.com/cxlite-demo/cxengine-patched:9.3
    
- Add required environment variables and values.  (Local, Group or Global)

  $DOCKER_AUTH_CONFIG

    Sample Value:
    {
        "auths":{
            "registry1.dso.mil":{
                "auth":"[base64 encoded username:cliSecret]"
            }
        }
    }

  $CX_LICENSE

    Sample Value [valid base64 encoded licence.cxl file]:
        //4AAFY...
        ...
        ...QAAAA==
  
- Rename sample-gitlab-ci.yml to gitlab-ci.yml.
- Trigger and review pipeline results.
